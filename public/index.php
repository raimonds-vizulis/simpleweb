<?php

session_start();

require_once dirname(__DIR__) . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

use SimpleWeb\Framework\Config\Config;
use SimpleWeb\Framework\Di\Container;

$container = new Container(new Config(
    require_once dirname(__DIR__) . '/app/etc/config.php'
));

require_once dirname(__DIR__) . '/app/functions.php';

/** @var \SimpleWeb\Framework\App\Bootstrap $app */
$app = $container->get(\SimpleWeb\Framework\App\Bootstrap::class);
$app->run();