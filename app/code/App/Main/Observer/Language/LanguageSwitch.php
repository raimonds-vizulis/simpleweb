<?php

namespace App\Main\Observer\Language;

use SimpleWeb\Framework\App\Http\RequestInterface;
use SimpleWeb\Framework\App\SessionInterface;
use SimpleWeb\Framework\Events\Model\Event;
use SimpleWeb\Framework\Events\Observer\ObserverInterface;

class LanguageSwitch implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        RequestInterface $request,
        SessionInterface $session
    ) {
        $this->request = $request;
        $this->session = $session;
    }

    /**
     * @param Event $event
     */
    public function execute(Event $event)
    {
        if ($lang = $this->request->getParam('lang')) {
            $this->session->set([
                '_lang' => $lang
            ]);
        }
    }
}