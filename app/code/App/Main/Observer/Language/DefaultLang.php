<?php

namespace App\Main\Observer\Language;

use SimpleWeb\Framework\App\SessionInterface;
use SimpleWeb\Framework\Events\Model\Event;
use SimpleWeb\Framework\Events\Observer\ObserverInterface;

class DefaultLang implements ObserverInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        SessionInterface $session
    ) {
        $this->session = $session;
    }

    public function execute(Event $event)
    {
        if (!$this->session->get('_lang')) {
            $this->session->set([
                '_lang' => 'lv'
            ]);
        }
    }
}