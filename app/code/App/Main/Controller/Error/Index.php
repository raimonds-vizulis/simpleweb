<?php

namespace App\Main\Controller\Error;

class Index extends \SimpleWeb\NotFound\Controller\Index\Index
{
    public function execute()
    {
        echo 'Custom 404 error page';
    }
}