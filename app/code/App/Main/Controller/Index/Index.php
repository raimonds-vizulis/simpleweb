<?php

namespace App\Main\Controller\Index;

use SimpleWeb\Framework\App\Http\Action;
use SimpleWeb\Framework\App\Http\RequestInterface;
use SimpleWeb\Framework\App\SessionInterface;
use SimpleWeb\Framework\App\View\Response\Type\Json;

class Index extends Action
{
    /**
     * @var Json
     */
    private $response;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        Json $response,
        RequestInterface $request,
        SessionInterface $session
    ) {
        $this->response = $response;
        $this->request = $request;
        $this->session = $session;
    }

    public function execute()
    {
        $this->response->render([
            'framework' => 'SimpleWeb',
            'version'   => $this->request->getParam('version'),
            'captcha'   => __('Text here'),
            'locale'    => $this->session->get('_lang')
        ]);
    }
}