<?php

namespace SimpleWeb\UrlRewrite\Observer;

use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Events\Model\Event;
use SimpleWeb\Framework\Events\Observer\ObserverInterface;
use SimpleWeb\Framework\Module\Loader;

class MatchRewrites implements ObserverInterface
{
    /**
     * @var array
     */
    private $rewrites = [];
    /**
     * @var Loader
     */
    private $loader;
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var XMLElement
     */
    private $xml;

    public function __construct(
        Loader $loader,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->loader = $loader;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->loadRewrites();
    }

    /**
     * @param Event $event
     */
    public function execute(Event $event)
    {
        /** @var \SimpleWeb\Framework\App\DataObject $request */
        $request = $event->getData('request');

        $requestPath = explode('?', $request->getData('REQUEST_URI'))[0];

        if (isset($this->rewrites[$requestPath])) {
            $request->setData('REQUEST_URI', $this->rewrites[$requestPath]);
        }
    }

    /**
     * Load module rewrites
     */
    private function loadRewrites()
    {
        foreach ($this->loader->getModules() as $module) {
            $rewriteFile = $this->directoryList->getPath(
                $module . '/etc/rewrites.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($rewriteFile, function ($file) {
                $this->parseXml(
                    $this->xml->loadXml($file)
                );
            });
        }
    }

    /**
     * @param SimpleXMLElement $xml
     * @throws \Exception
     */
    private function parseXml(SimpleXMLElement $xml)
    {
        foreach ($xml->children() as $rewrite) {
            /** @var SimpleXMLElement $rewrite */
            if (isset($this->rewrites[$rewrite->getAttribute('request')])) {
                throw new \Exception(
                    sprintf('Rewrite for "%s" already exists!', $rewrite->getAttribute('request'))
                );
            }

            $this->rewrites[$rewrite->getAttribute('request')] = $rewrite->getAttribute('target');
        }
    }
}