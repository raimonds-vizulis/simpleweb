<?php

namespace SimpleWeb\Framework\Di;

interface ContainerInterface
{
    /**
     * Get class instance
     *
     * @param $class
     * @param array $arguments
     * @return mixed
     */
    public function get($class, array $arguments = []);

    /**
     * Create new class instance
     *
     * @param $class
     * @param array $arguments
     * @return mixed
     */
    public function make($class, array $arguments = []);

    /**
     * Call method on new class instance
     *
     * @param $class
     * @param $method
     * @param array $arguments
     * @return mixed
     */
    public function call($class, $method, array $arguments = []);
}