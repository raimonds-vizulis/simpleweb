<?php

namespace SimpleWeb\Framework\Di\Util;

class Instances
{
    /**
     * @var array
     */
    private $instances = [];

    /**
     * @var array
     */
    private $interfaces = [];

    /**
     * @var array
     */
    private $singletons = [];

    /**
     * @var array
     */
    private $extends = [];

    /**
     * Set instance
     *
     * @param $name
     * @param $object
     * @return $this
     */
    public function setInstance($name, $object = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $instance) {
                if (!isset($this->instances[$key])) {
                    $this->instances[$key] = $instance;
                }
            }
        } else {
            if (!isset($this->instances[$name])) {
                $this->instances[$name] = $object;
            }
        }

        return $this;
    }

    /**
     * Get instance
     *
     * @param $name
     * @return mixed|null
     */
    public function getInstance($name = null)
    {
        return $name ? (isset($this->instances[$name]) ? $this->instances[$name] : null) : $this->instances;
    }

    /**
     * Set interfaces
     *
     * @param $interface
     * @param null $class
     * @return $this
     */
    public function setInterface($interface, $class = null)
    {
        if (is_array($interface)) {
            foreach ($interface as $key => $instance) {
                if (!isset($this->instances[$key])) {
                    $this->interfaces[$key] = $instance;
                }
            }
        } else {
            if (!isset($this->interfaces[$interface])) {
                $this->interfaces[$interface] = $class;
            }
        }

        return $this;
    }

    /**
     * Get interfaces
     *
     * @param null $interface
     * @return array
     */
    public function getInterface($interface = null)
    {
        return $interface ?
            (isset($this->interfaces[$interface]) ? $this->interfaces[$interface] : null) :
            $this->interfaces;
    }

    /**
     * Register class as singleton
     *
     * @param $class
     * @return $this
     */
    public function registerClassAsSingleton($class)
    {
        if (!in_array($class, $this->singletons)) {
            $this->singletons[] = $class;
        }

        return $this;
    }

    /**
     * Is class singleton
     *
     * @param $class
     * @return bool
     */
    public function isClassSingleton($class)
    {
        return in_array($class, $this->singletons);
    }

    /**
     * @param $object
     * @return array
     */
    public function getClassName($object)
    {
        if ($class = $this->getInterface($object)) {
            $object = $class;
        }

        if ($extend = $this->getExtendForClass($object)) {
            return $extend;
        }

        return $object;
    }

    /**
     * @param $for
     * @param null $with
     */
    public function addExtend($for, $with = null)
    {
        if (is_array($for)) {
            foreach ($for as $_for => $_with) {
                if (!isset($this->extends[$_for])) {
                    $this->extends[$_for] = $_with;
                }
            }
        } else {
            if (!isset($this->extends[$for])) {
                $this->extends[$for] = $with;
            }
        }
    }

    /**
     * @param $class
     * @return mixed
     */
    public function getExtendForClass($class)
    {
        if (isset($this->extends[$class])) {
            return $this->extends[$class];
        }

        return $class;
    }
}