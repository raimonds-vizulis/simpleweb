<?php

namespace SimpleWeb\Framework\Di\Util;

use ReflectionClass;
use ReflectionFunctionAbstract;
use ReflectionMethod;
use ReflectionParameter;

class Parser
{
    /**
     * @var Instances
     */
    private $instances;

    public function __construct(
        Instances $instances
    ) {
        $this->instances = $instances;
    }

    /**
     * Retrieve class
     *
     * @param $class
     * @param array $arguments
     * @return object
     * @throws \Exception
     */
    public function retrieveClass($class, array $arguments = [])
    {
        $reflectionClass = new ReflectionClass($class);
        $className = $reflectionClass->getName();

        if (
            !$reflectionClass->isInstantiable() &&
            $reflectionClass->isInterface()
        ) {
            throw new \Exception('"' . $className. '" is not something that i can initialize!');
        }

        $data = $reflectionClass->getConstructor();

        if ($data) {
            return $reflectionClass->newInstanceArgs(
                array_merge(
                    $this->retrieveArgs($data),
                    $arguments
                )
            );
        }

        return new $className;
    }

    /**
     * Retrieve class method
     *
     * @param $class
     * @param ReflectionMethod $method
     * @param array $arguments
     * @return mixed
     */
    public function retrieveMethod($class, ReflectionMethod $method, array $arguments = [])
    {
        if (
            !empty($arguments) &&
            count($method->getParameters()) == count($arguments)
        ) {
            return $method->invokeArgs(
                $this->retrieveClass($class),
                $arguments
            );
        } else if (
            !empty($arguments) &&
            count($method->getParameters()) > count($arguments)
        ) {
            $args = $this->retrieveArgs($method);

            for ($i = 0; $i < count($arguments); $i++) {
                unset($args[$i]);
            }

            return $method->invokeArgs(
                $this->retrieveClass($class),
                array_merge(
                    $arguments,
                    $args
                )
            );
        }

        return $method->invokeArgs(
            $this->retrieveClass($class),
            array_merge(
                $this->retrieveArgs($method),
                $arguments
            )
        );
    }

    /**
     * Retrieve constructor / class method arguments
     *
     * @param ReflectionFunctionAbstract $data
     * @return array
     */
    protected function retrieveArgs(ReflectionFunctionAbstract $data)
    {
        $params = $data->getParameters();
        $args = [];

        if ($params) {
            foreach ($params as $param) {
                /** @var ReflectionParameter $param */
                if (!($class = $param->getClass())) {
                    continue;
                }

                $className = $this->instances->getClassName(
                    $class->getName()
                );

                if (!isset($className)) {
                    continue;
                }

                if (
                    $this->instances->isClassSingleton($className)
                ) {
                    $args[] = $this->instances->getInstance($className) ?: $this->createSingleton($className);
                    continue;
                }

                $args[] = $this->retrieveClass($className);
            }
        }

        return $args;
    }

    /**
     * @param $className
     * @return mixed|null
     */
    protected function createSingleton($className)
    {
        $this->instances->setInstance($className, $this->retrieveClass($className));

        return $this->instances->getInstance($className);
    }

    /**
     * @return Instances
     */
    public function getInstances()
    {
        return $this->instances;
    }

    public function setInstances(Instances $instances)
    {
        $this->instances = $instances;

        return $this;
    }
}
