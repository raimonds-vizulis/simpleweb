<?php

namespace SimpleWeb\Framework\Di;

use ReflectionMethod;
use SimpleWeb\Framework\Config\Config;
use SimpleWeb\Framework\Config\ConfigInterface;
use SimpleWeb\Framework\Di\Util\Instances;
use SimpleWeb\Framework\Di\Util\Parser;

class Container implements ContainerInterface
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var array
     */
    private $instances;

    /**
     * @var Container
     */
    private static $instance = null;

    public function __construct(Config $config)
    {
        $this->instances = new Instances;

        $this->instances
            ->setInstance([
                Container::class => $this,
                Config::class => $config
            ])
            ->setInterface([
                ContainerInterface::class => Container::class,
                ConfigInterface::class => Config::class
            ]);

        $this->configure($config->get('container'));

        $this->parser = new Parser($this->instances);

        self::$instance = $this;
    }

    /**
     * Get class instance
     *
     * @param $class
     * @param array $arguments
     * @return mixed
     */
    public function get($class, array $arguments = [])
    {
        $class = $this->instances->getClassName($class);

        if (!$this->instances->getInstance($class)) {
            $this->instances->setInstance(
                $class,
                $this->parser->retrieveClass($class, $arguments)
            );
        }

        return $this->instances->getInstance($class);
    }

    /**
     * Create new class instance
     *
     * @param $class
     * @param array $arguments
     * @return mixed
     */
    public function make($class, array $arguments = [])
    {
        return $this->parser->retrieveClass(
            $this->instances->getClassName($class),
            $arguments
        );
    }

    /**
     * Call method on new class instance
     *
     * @param $class
     * @param $method
     * @param array $arguments
     * @return mixed
     */
    public function call($class, $method, array $arguments = [])
    {
        $class = $this->instances->getClassName($class);
        $method = new ReflectionMethod($class, $method);

        return $this->parser->retrieveMethod($class, $method, $arguments);
    }

    /**
     * Configure container
     * TODO: Add more configurable possibilities
     *
     * @param array $data
     */
    public function configure(array $data)
    {
        if (isset($data['interfaces'])) {
            foreach ($data['interfaces'] as $interface => $class) {
                $this->instances->setInterface($interface, $class);
            }
        }

        if (isset($data['singletons'])) {
            foreach ($data['singletons'] as $class) {
                $this->instances->registerClassAsSingleton($class);
            }
        }
    }

    /**
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @return Container
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            throw new \Exception('Container has not been initialized, yet!');
        }

        return self::$instance;
    }
}
