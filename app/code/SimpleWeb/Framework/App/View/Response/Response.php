<?php

namespace SimpleWeb\Framework\App\View\Response;

abstract class Response
{
    abstract public function render(array $data = []);
}