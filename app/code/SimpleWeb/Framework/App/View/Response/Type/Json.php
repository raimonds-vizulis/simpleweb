<?php

namespace SimpleWeb\Framework\App\View\Response\Type;

use SimpleWeb\Framework\App\View\Response\Response;

class Json extends Response
{
    public function render(array $data = [])
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}