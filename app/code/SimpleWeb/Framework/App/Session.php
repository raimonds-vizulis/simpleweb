<?php

namespace SimpleWeb\Framework\App;

class Session implements SessionInterface
{
    /**
     * Set data in session
     *
     * @param $field
     * @param null $value
     * @return $this
     */
    public function set($field, $value = null)
    {
        if (is_array($field)) {
            foreach ($field as $key => $value) {
                $_SESSION[$key] = $value;
            }

            return $this;
        }

        $_SESSION[$field] = $value;

        return $this;
    }

    /**
     * Get data from session
     *
     * @param $key
     * @param bool $unsetAfterReturn
     * @return null
     */
    public function get($key, $unsetAfterReturn = false)
    {
        if (isset($_SESSION[$key])) {
            if ($unsetAfterReturn) {
                $var = $_SESSION[$key];
                unset($_SESSION[$key]);

                return $var;
            }

            return $_SESSION[$key];
        }

        return null;
    }

    /**
     * Unset data from session
     *
     * @param $field
     */
    public function remove($field)
    {
        if (is_array($field)) {
            foreach ($field as $key) {
                $this->remove($key);
            }
        }

        if (isset($_SESSION[$field])) {
            unset($_SESSION[$field]);
        }
    }

    /**
     * Dump session data
     *
     * @return mixed
     */
    public function dump()
    {
        return $_SESSION;
    }

    /**
     * Destroy session
     *
     * @return void
     */
    public function destroy()
    {
        session_destroy();
    }
}