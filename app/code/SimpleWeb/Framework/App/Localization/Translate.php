<?php

namespace SimpleWeb\Framework\App\Localization;

use SimpleWeb\Framework\App\SessionInterface;
use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\Events\Model\EventDispatcherInterface;

class Translate
{
    /**
     * @var array
     */
    private $translations = [];

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var Filesystem
     */
    private $filesystem;

    public function __construct(
        SessionInterface $session,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->session = $session;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->eventDispatcher = $eventDispatcher;

        $this->loadTranslations();
    }

    /**
     * @param $string
     * @param null $parameters
     * @return mixed
     */
    public function translate($string, $parameters = null)
    {
        if ($parameters) {
            if (count($parameters) > 1) {
                vsprintf($this->checkTranslation($string), $parameters);
            }

            return sprintf($this->checkTranslation($string), $parameters[0]);
        }

        return $this->checkTranslation($string);
    }

    /**
     * Load translation file
     * @return void
     */
    private function loadTranslations()
    {
        $this->eventDispatcher->dispatch('load_translations');
        $langCode = $this->session->get('_lang');

        if (!$langCode) {
            return;
        }

        $file = $this->directoryList->getPath(
            'l10n/' . $langCode . '.json',
            DirectoryList::APP
        );

        $this->filesystem->loadFile($file, function ($file) {
            $this->translations = json_decode($file, true);
        }, true);
    }

    /**
     * @param $string
     * @return mixed
     */
    private function checkTranslation($string)
    {
        if (isset($this->translations[$string])) {
            return $this->translations[$string];
        }

        return $string;
    }
}