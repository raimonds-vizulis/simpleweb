<?php

namespace SimpleWeb\Framework\App\Http;

use SimpleWeb\Framework\App\DataObject;
use SimpleWeb\Framework\App\Http\Util\Routes;
use SimpleWeb\Framework\Di\ContainerInterface;
use SimpleWeb\Framework\Events\Model\EventDispatcherInterface;

class Request implements RequestInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var Routes
     */
    private $routes;

    /**
     * @var DataObject
     */
    private $request;

    /**
     * Request constructor.
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $eventDispatcher
     * @param Routes $routes
     * @param DataObject $dataObject
     */
    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $eventDispatcher,
        Routes $routes,
        DataObject $dataObject
    ) {
        $this->container = $container;
        $this->eventDispatcher = $eventDispatcher;
        $this->routes = $routes;
        $this->request = $dataObject;

        $this->parseRequest();
    }

    private function parseRequest()
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            return;
        }

        $this->eventDispatcher->dispatch('start_parse_request');

        if (empty($this->request->getData())) {
            /** @var DataObject $request */
            $request = $this->container->make(DataObject::class)->addData($_SERVER);

            $this->request->setData('url', sprintf(
                '%s://%s',
                ($request->getData('HTTPS') && $request->getData('HTTPS') != 'off') ? 'https' : 'http',
                $request->getData('SERVER_NAME')
            ));

            $this->request->setData('self', $this->request->getData('url') . $request->getData('REQUEST_URI'));

            // @TODO: Needs escape for referer as it can be vulnerability for injection
            $this->request->setData('referer', $request->getData('HTTP_REFERER'));

            $this->request->setData(
                $this->routes->matchRoute($request)
            );
        }
    }

    /**
     * Get url parameter
     *
     * @TODO: Needs escape for return
     *
     * @param $parameter
     * @param null $default
     * @return null
     */
    public function getParam($parameter, $default = null)
    {
        if ($url = $this->request->getData('self')) {
            if (preg_match('#' . $parameter . '/(.*)#', $url, $match)) {
                list ($param) = explode('/', $match[1]);
                return $param;
            } else if (isset($_GET[$parameter])) {
                return $_GET[$parameter];
            }

            return $default;
        }

        return $default;
    }

    /**
     * Checks if request method is POST
     *
     * @return bool
     */
    public function isPost()
    {
        return strtolower($_SERVER['REQUEST_METHOD']) === 'post';
    }

    /**
     * Get $_POST data
     *
     * @param null $key
     * @return mixed
     */
    public function getPostData($key = null)
    {
        if ($this->isPost() && !empty($_POST)) {
            /** @var DataObject $post */
            $post = $this->container->make(DataObject::class)->addData($_POST);

            if ($key && $data = $post->getData($key)) {
                return $data;
            }

            return $post;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->request->getData('route');
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->request->getData('controller');
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->request->getData('action');
    }
}