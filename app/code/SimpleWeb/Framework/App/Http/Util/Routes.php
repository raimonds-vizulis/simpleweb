<?php

namespace SimpleWeb\Framework\App\Http\Util;

use SimpleWeb\Framework\App\DataObject;
use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Events\Model\EventDispatcherInterface;
use SimpleWeb\Framework\Module\Loader;

class Routes
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var Loader
     */
    private $loader;
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var array
     */
    private $routes = [];

    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var XMLElement
     */
    private $xml;

    /**
     * Routes constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param Loader $loader
     * @param DirectoryList $directoryList
     * @param Filesystem $filesystem
     * @param XMLElement $xml
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        Loader $loader,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->loader = $loader;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->loadRoutes();
    }

    /**
     * @param DataObject $request
     * @return array
     */
    public function matchRoute(DataObject $request)
    {
        $this->eventDispatcher->dispatch('start_matching_routes', ['request' => $request]);
        $data = [];

        $requestPath = explode('?', trim($request->getData('REQUEST_URI'), '/'));
        $requestPath = explode('/', $requestPath[0]);

        $data['route'] = isset($requestPath[0]) && $requestPath[0] != '' ? $this->getRoute($requestPath[0]) : null;

        if ($data['route'] == null) {
            $data = [
                'route' => 'SimpleWeb\\NotFound\\',
                'controller' => 'Index',
                'action' => 'Index'
            ];
            return $data;
        }

        $data['controller'] = isset($requestPath[1]) && $requestPath[1] != '' ? ucfirst($requestPath[1]) : 'Index';
        $data['action'] = isset($request_path[2]) && $request_path[2] != '' ? ucfirst($request_path[2]) : 'Index';

        return $data;
    }

    /**
     * Load module routes
     */
    private function loadRoutes()
    {
        foreach ($this->loader->getModules() as $module) {
            $routeFile = $this->directoryList->getPath(
                $module . '/etc/routes.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($routeFile, function ($file) {
                $this->parseXml(
                    $this->xml->loadXml($file)
                );
            });
        }
    }

    /**
     * @param SimpleXMLElement $xml
     * @throws \Exception
     */
    private function parseXml(SimpleXMLElement $xml)
    {
        foreach ($xml->children() as $route) {
            /** @var SimpleXMLElement $route */
            if ($routeName = $this->routeExists($route->getAttribute('name'))) {
                throw new \Exception(
                    sprintf('Route with name %s already exists!', $routeName)
                );
            }

            $this->routes[$route->getAttribute('name')] =
                str_replace('_', '\\', $route->getAttribute('module'));
        }
    }

    /**
     * @param $name
     * @return mixed|null
     */
    private function getRoute($name)
    {
        if ($route = $this->routeExists($name)) {
            return $route . '\\';
        }

        return null;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    private function routeExists($name)
    {
        return isset($this->routes[$name]) ? $this->routes[$name] : null;
    }
}