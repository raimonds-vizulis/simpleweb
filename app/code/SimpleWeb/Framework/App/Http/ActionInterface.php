<?php

namespace SimpleWeb\Framework\App\Http;

interface ActionInterface
{
    public function execute();
}