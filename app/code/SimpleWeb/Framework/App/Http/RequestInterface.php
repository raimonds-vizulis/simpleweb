<?php

namespace SimpleWeb\Framework\App\Http;

interface RequestInterface
{
    /**
     * @param $parameter
     * @param null $default
     * @return mixed
     */
    public function getParam($parameter, $default = null);

    /**
     * Checks if request method is POST
     *
     * @return bool
     */
    public function isPost();

    /**
     * Get $_POST data
     *
     * @param null $key
     * @return mixed
     */
    public function getPostData($key = null);

    /**
     * @return mixed
     */
    public function getModule();

    /**
     * @return mixed
     */
    public function getController();

    /**
     * @return mixed
     */
    public function getAction();
}