<?php

namespace SimpleWeb\Framework\App;

class DataObject
{
    /**
     * @var array
     */
    private $_data = [];

    /**
     * @var array
     */
    private $_origData = [];

    /**
     * @param $method
     * @param $arg
     * @return $this|array|mixed|null
     */
    public function __call($method, $arg) {
        switch (substr($method, 0, 3)) {
            case 'get':
                $key = $this->fromCamelCase(substr($method, 3));
                $data = $this->getData($key);
                return $data;
            case 'set' :
                $key = $this->fromCamelCase(substr($method, 3));
                $this->setData($key, isset($arg[0]) ? $arg[0] : null);
                return $this;
            case 'unset':
                $key = $this->fromCamelCase(substr($method, 5));
                $this->unsetData($key);
                return $this;
        }

        return $this;
    }

    /**
     * Add data to object
     *
     * @param $data
     * @return $this
     */
    public function addData($data)
    {
        $this->_origData = $data;
        $this->_data = $data;

        return $this;
    }

    /**
     * Set object data
     *
     * @param $field
     * @param null $value
     * @return $this
     */
    public function setData($field, $value = null)
    {
        if (is_array($field)) {
            foreach ($field as $key => $val) {
                $this->_data[$key] = $val;
            }
        } else {
            $this->_data[$field] = $value;
        }

        return $this;
    }

    /**
     * Get data from object
     * Use a/b/c to get data within array
     *
     * @param null $field
     * @return array|mixed|null
     */
    public function getData($field = null)
    {
        if ($field) {
            $path = explode('/', $field);
            $config = $this->_data;

            foreach ($path as $field) {
                if (!isset($config[$field])) {
                    return null;
                }

                $config = $config[$field];
            }

            return $config;
        }

        return $this->_data;
    }

    /**
     * Unset data from object
     *
     * @param $key array|string
     * @return $this
     */
    public function unsetData($key)
    {
        if (is_array($key)) {
            foreach ($key as $field) {
                $this->unsetData($field);
            }
        }

        if (isset($this->_data[$key])) {
            unset($this->_data[$key]);
        }

        return $this;
    }

    /**
     * Checks if current data is same as current
     *
     * @param $key
     * @return bool
     */
    public function hasDataChanged($key = null)
    {
        if (!empty($this->_origData)) {
            if ($key) {
                if (
                    isset($this->_origData[$key]) &&
                    isset($this->_data[$key]) &&
                    $this->_origData[$key] != $this->_data[$key]
                ) {
                    return true;
                }

                return false;
            }

            if ($this->_origData != $this->_data) {
                return true;
            }
        }

        return false;
    }

    /**
     * From camel case to dashed string
     *
     * @param $string
     * @return string
     */
    private function fromCamelCase($string)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $string)), '_');
    }
}