<?php

namespace SimpleWeb\Framework\App;

interface SessionInterface
{
    /**
     * Set data in session
     *
     * @param $field
     * @param null $value
     * @return $this
     */
    public function set($field, $value = null);

    /**
     * Get data from session
     *
     * @param $key
     * @param bool $unsetAfterReturn
     * @return null
     */
    public function get($key, $unsetAfterReturn = false);

    /**
     * Unset data from session
     *
     * @param $field
     */
    public function remove($field);

    /**
     * Dump session data
     *
     * @return mixed
     */
    public function dump();

    /**
     * Destroy session
     *
     * @return void
     */
    public function destroy();
}