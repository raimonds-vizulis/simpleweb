<?php

namespace SimpleWeb\Framework\App;

class Singleton
{
    /**
     * @var Singleton
     */
    private static $instance;

    public function __construct()
    {
        self::$instance = $this;
    }

    /**
     * @return Singleton
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            throw new \Exception('Class has not been initialized yet!');
        }

        return self::$instance;
    }
}