<?php

namespace SimpleWeb\Framework\App;

use SimpleWeb\Framework\App\Http\RequestInterface;
use SimpleWeb\Framework\Code\ClassExtends;
use SimpleWeb\Framework\Di\ContainerInterface;
use SimpleWeb\Framework\Events\Model\EventDispatcherInterface;
use SimpleWeb\Framework\Module\Loader;

class Bootstrap
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        Loader $loader,
        ClassExtends $classExtends,
        ContainerInterface $container,
        EventDispatcherInterface $eventDispatcher,
        RequestInterface $request
    ) {
        $this->container = $container;
        $this->eventDispatcher = $eventDispatcher;
        $this->request = $request;
    }

    public function run()
    {
        $this->eventDispatcher->dispatch('start_processing');

        $class = $this->request->getModule() . 'Controller\\' .
            $this->request->getController() . '\\' . $this->request->getAction();
        $this->container->call($class, 'execute');
    }
}