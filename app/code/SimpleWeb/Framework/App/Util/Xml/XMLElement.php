<?php

namespace SimpleWeb\Framework\App\Util\Xml;

use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\Di\ContainerInterface;

class XMLElement
{
    const FILE   = 0;
    const STRING = 1;

    /**
     * @var |Container
     */
    private $container;

    /**
     * @var Filesystem
     */
    private $filesystem;

    public function __construct(
        ContainerInterface $container,
        Filesystem $filesystem
    ) {
        $this->container = $container;
        $this->filesystem = $filesystem;
    }

    /**
     * @param $content
     * @param int $type
     * @return null|SimpleXMLElement
     */
    public function loadXml($content, $type = self::FILE)
    {
        switch ($type) {
            case self::FILE:
                if ($this->filesystem->exists($content)) {
                    return simplexml_load_file($content, SimpleXMLElement::class);
                }
                break;

            case self::STRING:
                return simplexml_load_string($content, SimpleXMLElement::class);
                break;
        }

        return null;
    }
}