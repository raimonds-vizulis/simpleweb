<?php

namespace SimpleWeb\Framework\App\Util\Xml;

class SimpleXMLElement extends \SimpleXMLElement
{
    /**
     * @param $attribute
     * @return null|string
     */
    public function getAttribute($attribute)
    {
        $object = $this;

        return isset($object[$attribute]) ? (string) $object[$attribute] : null;
    }
}