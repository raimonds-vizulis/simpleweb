<?php

namespace SimpleWeb\Framework\App\Util;

class Filesystem extends \Symfony\Component\Filesystem\Filesystem
{
    /**
     * @param $file
     * @param \Closure|null $callback
     * @param bool $getFileContent
     * @return bool|mixed|string
     */
    public function loadFile($file, \Closure $callback = null, $getFileContent = false)
    {
        if (!$this->exists($file)) {
            return false;
        }

        if ($callback) {
            if ($getFileContent) {
                $file = file_get_contents($file);
            }
            
            return $callback($file);
        }

        return file_get_contents($file);
    }
}