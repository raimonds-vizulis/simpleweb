<?php

namespace SimpleWeb\Framework\App\Util;

use SimpleWeb\Framework\Config\ConfigInterface;

class DirectoryList
{
    const ROOT = 'root';
    const APP  = 'app';
    const CODE = 'code';
    const MISC = 'misc';
    const TMP  = 'tmp';

    const APP_PATH  = 'app';
    const CODE_PATH = 'app/code';
    const MISC_PATH = 'app/etc';
    const TMP_PATH  = 'var';

    /**
     * @var string
     */
    private $rootDir;

    public function __construct(
        ConfigInterface $config
    ) {
        $this->rootDir = $config->get('config', 'ROOT_DIR');
    }

    /**
     * @return array
     */
    public function getPathList()
    {
        $root = $this->rootDir;

        return [
            self::ROOT => $root,
            self::APP  => $root . '/' . self::APP_PATH,
            self::CODE => $root . '/' . self::CODE_PATH,
            self::MISC => $root . '/' . self::MISC_PATH,
            self::TMP  => $root . '/' . self::TMP_PATH
        ];
    }

    /**
     * @param string $path
     * @param null $type
     * @return mixed|string
     */
    public function getPath($path = '', $type = null)
    {
        if ($type) {
            $pathList = $this->getPathList();

            if (isset($pathList[$type])) {
                return $pathList[$type] . '/' . $path;
            }
        }

        if (empty($path)) {
            return $this->rootDir;
        }

        return $this->rootDir . '/' . $path;
    }
}