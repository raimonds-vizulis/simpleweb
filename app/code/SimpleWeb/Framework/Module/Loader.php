<?php

namespace SimpleWeb\Framework\Module;

use SimpleWeb\Framework\App\DataObject;
use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Di\ContainerInterface;

class Loader
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var Register
     */
    private $register;

    /**
     * @var array
     */
    private $modules = [];

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var XMLElement
     */
    private $xml;

    public function __construct(
        ContainerInterface $container,
        DirectoryList $directoryList,
        Register $register,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->container = $container;
        $this->directoryList = $directoryList;
        $this->register = $register;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->loadModules();
    }

    /**
     * Load modules
     */
    public function loadModules()
    {
        foreach ($this->register->getModules() as $module) {
            $moduleFile = $this->directoryList->getPath(
                $module . '/etc/module.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($moduleFile, function ($file) {
                $content = $this->parseModuleFile(
                    $this->xml->loadXml($file)
                );

                if (!$content->getData('enabled')) {
                    return;
                }

                $this->addModule($content);
            });
        }

        $this->sortSequence();
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return array_keys($this->modules);
    }

    /**
     * @param SimpleXMLElement $content
     * @return DataObject
     * @throws \Exception
     */
    private function parseModuleFile(SimpleXMLElement $content)
    {
        /** @var DataObject $data */
        $data = $this->container->make(DataObject::class);

        if ($content->count() != 1) {
            throw new \Exception('No module element in XML configuration!');
        }

        foreach ($content->children() as $child) {
            /** @var SimpleXMLElement $child */
            $sequenceData = [];

            if ($child->count()) {
                foreach ($child->children() as $sequence) {
                   /** @var SimpleXMLElement $sequence */
                   if (!$sequence->count()) {
                       continue;
                   }

                   foreach ($sequence->children() as $module) {
                       /** @var SimpleXMLElement $module */
                       $tmpName = explode('_', $module->getAttribute('name'), 2);
                       $sequenceData[] = $tmpName[0] . '/' . $tmpName[1];
                   }
                }
            }

            $tmpName = explode('_', $child->getAttribute('name'), 2);

            $data->setData([
                'name' => $tmpName[0] . '/' . $tmpName[1],
                'enabled' => strtolower($child->getAttribute('enabled')) == 'true'
            ]);

            if (!empty($sequenceData)) {
                $data->setData('sequence', $sequenceData);
            }
        }

        return $data;
    }

    /**
     * @param DataObject $data
     * @throws \Exception
     */
    private function addModule(DataObject $data)
    {
        if (isset($this->modules[$data->getData('name')])) {
            throw new \Exception('Module "' . $data->getData('name') . '" already registered!');
        }

        $this->modules[$data->getData('name')] = $data;
    }

    /**
     * Sort modules by sequence
     * @return void
     */
    private function sortSequence()
    {
        ksort($this->modules);
        $list = [];

        foreach ($this->modules as $module => $data) {
            $list[] = [
                'name' => $data->getName(),
                'sequence' => $data->getSequence() ?: []
            ];
        }

        $total = count($list);

        for ($i = 0; $i < $total - 1; $i++) {
            for ($j = $i; $j < $total; $j++) {
                if (in_array($list[$j]['name'], $list[$i]['sequence'])) {
                    $temp = $list[$i];
                    $list[$i] = $list[$j];
                    $list[$j] = $temp;
                }
            }
        }

        $result = [];

        foreach ($list as $item) {
            $result[$item['name']] = $this->modules[$item['name']];
        }

        $this->modules = $result;
    }
}