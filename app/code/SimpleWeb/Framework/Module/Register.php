<?php

namespace SimpleWeb\Framework\Module;

class Register
{
    const MODULE = 'module';
    const THEME  = 'theme';

    /**
     * @var array
     */
    private static $modules = [];

    /**
     * @var array
     */
    private static $themes  = [];

    /**
     * @param $type
     * @param $name
     */
    public static function register($type, $name)
    {
        $tmpName = explode('_', $name, 2);
        $name = $tmpName[0] . '/' . $tmpName[1];

        if ($type == self::MODULE) {
            if (!in_array($name, self::$modules)) {
                self::$modules[] = $name;
            }
        } else if ($type == self::THEME) {
            if (!in_array($name, self::$themes)) {
                self::$themes[] = $name;
            }
        }
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return self::$modules;
    }

    /**
     * @return array
     */
    public function getThemes()
    {
        return self::$themes;
    }
}