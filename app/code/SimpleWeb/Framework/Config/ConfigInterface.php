<?php

namespace SimpleWeb\Framework\Config;

interface ConfigInterface
{
    /**
     * @param $section
     * @param null $field
     * @return mixed
     */
    public function get($section, $field = null);
}