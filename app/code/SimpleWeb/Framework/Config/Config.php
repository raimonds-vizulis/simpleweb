<?php

namespace SimpleWeb\Framework\Config;

class Config implements ConfigInterface
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var Config
     */
    private static $instance;

    /**
     * Config constructor.
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        foreach ($data as $section => $sectionData) {
            if (isset($this->data[$section])) {
                throw new \Exception('Duplicate configuration!');
            }

            $this->data[$section] = $sectionData;
        }

        self::$instance = $this;
    }

    /**
     * @param $section
     * @param null $field
     * @return mixed|null
     */
    public function get($section, $field = null)
    {
        if (isset($this->data[$section])) {
            return $field ? $this->data[$section][$field] : $this->data[$section];
        }

        return null;
    }

    /**
     * @return Config
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            throw new \Exception('Config has not been initialized, yet!');
        }

        return self::$instance;
    }
}