<?php

namespace SimpleWeb\Framework\Code;

use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Di\Container;
use SimpleWeb\Framework\Module\Loader;

class ClassExtends
{
    /**
     * @var Loader
     */
    private $loader;

    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var XMLElement
     */
    private $xml;

    /**
     * ClassExtends constructor.
     * @param Loader $loader
     * @param DirectoryList $directoryList
     * @param Container $container
     * @param Filesystem $filesystem
     * @param XMLElement $xml
     */
    public function __construct(
        Loader $loader,
        DirectoryList $directoryList,
        Container $container,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->loader = $loader;
        $this->directoryList = $directoryList;
        $this->container = $container;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->loadExtends();
    }

    private function loadExtends()
    {
        foreach ($this->loader->getModules() as $module) {
            $extendsFile = $this->directoryList->getPath(
                $module . '/etc/extends.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($extendsFile, function ($file) {
                $this->parseXml(
                    $this->xml->loadXml($file)
                );
            });
        }
    }

    /**
     * @param SimpleXMLElement $content
     */
    private function parseXml(SimpleXMLElement $content)
    {
        $instances = $this->container->getParser()->getInstances();

        foreach ($content->children() as $child) {
            /** @var SimpleXMLElement $child */
            $instances->addExtend(
                $child->getAttribute('for'),
                $child->getAttribute('with')
            );
        }
    }
}