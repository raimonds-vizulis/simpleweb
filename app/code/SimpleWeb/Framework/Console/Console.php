<?php

namespace SimpleWeb\Framework\Console;

use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Console\Exception\CommandExist;
use SimpleWeb\Framework\Di\ContainerInterface;
use SimpleWeb\Framework\Module\Loader;
use Symfony\Component\Console\Application;

class Console
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $commands = [];

    /**
     * @var Loader
     */
    private $loader;

    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var XMLElement
     */
    private $xml;

    public function __construct(
        ContainerInterface $container,
        Loader $loader,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->container = $container;
        $this->loader = $loader;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->getCommands();
        $this->loadConsole();
    }

    /**
     * Init console
     */
    protected function loadConsole()
    {
        $console = new Application;

        foreach ($this->commands as $command) {
            $console->add($command);
        }

        $console->run();
    }

    /**
     * Load module commands
     */
    protected function getCommands()
    {
        foreach ($this->loader->getModules() as $module) {
            $commandsFile = $this->directoryList->getPath(
                $module . '/etc/commands.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($commandsFile, function ($file) {
                $this->parseXml(
                    $this->xml->loadXml($file)
                );
            });
        }
    }

    /**
     * @param SimpleXMLElement $xml
     * @throws CommandExist
     */
    private function parseXml(SimpleXMLElement $xml)
    {
        foreach ($xml->children() as $command) {
            /** @var SimpleXMLElement $command */
            if (
                isset($this->commands[$command->getAttribute('name')])
            ) {
                throw new CommandExist(
                    sprintf('Command with name: %s already exists!', $command->getAttribute('name'))
                );
            }

            $this->commands[$command->getAttribute('name')] =
                $this->container->make($command->getAttribute('class'));
        }
    }
}