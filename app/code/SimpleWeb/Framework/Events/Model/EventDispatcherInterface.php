<?php

namespace SimpleWeb\Framework\Events\Model;

interface EventDispatcherInterface
{
    /**
     * @param $eventName
     * @param array $parameters
     * @return mixed|void
     */
    public function dispatch($eventName, array $parameters = []);
}