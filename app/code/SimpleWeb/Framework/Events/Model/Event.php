<?php

namespace SimpleWeb\Framework\Events\Model;

use SimpleWeb\Framework\App\DataObject;

class Event extends DataObject
{
    public function getEventName()
    {
        return $this->getData('event_name');
    }
}