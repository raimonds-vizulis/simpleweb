<?php

namespace SimpleWeb\Framework\Events\Model;

use SimpleWeb\Framework\App\Util\DirectoryList;
use SimpleWeb\Framework\App\Util\Filesystem;
use SimpleWeb\Framework\App\Util\Xml\SimpleXMLElement;
use SimpleWeb\Framework\App\Util\Xml\XMLElement;
use SimpleWeb\Framework\Di\ContainerInterface;
use SimpleWeb\Framework\Events\Exception\EventExists;
use SimpleWeb\Framework\Module\Loader;

class EventDispatcher implements EventDispatcherInterface
{
    /**
     * @var array
     */
    private $observers = [];

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Loader
     */
    private $loader;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var XMLElement
     */
    private $xml;

    /**
     * EventDispatcher constructor.
     * @param ContainerInterface $container
     * @param Loader $loader
     * @param DirectoryList $directoryList
     * @param Filesystem $filesystem
     * @param XMLElement $xml
     */
    public function __construct(
        ContainerInterface $container,
        Loader $loader,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        XMLElement $xml
    ) {
        $this->container = $container;
        $this->loader = $loader;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->xml = $xml;

        $this->loadModuleEvents();
    }

    /**
     * @param $eventName
     * @param array $parameters
     * @return mixed|void
     */
    public function dispatch($eventName, array $parameters = [])
    {
        if (isset($this->observers[$eventName])) {
            foreach ($this->observers[$eventName] as $name => $class) {
                /** @var Event $event */
                $event = $this->container->make(Event::class);

                $data = ['event_name' => $eventName];

                if (!empty($parameters)) {
                    foreach ($parameters as $parameter => $value) {
                        $data[$parameter] = $value;
                    }
                }

                $event->addData($data);

                $this->container->call($class, 'execute', [$event]);
            }
        }
    }

    private function loadModuleEvents()
    {
        foreach ($this->loader->getModules() as $module) {
            $eventFile = $this->directoryList->getPath(
                $module . '/etc/events.xml',
                DirectoryList::CODE
            );

            $this->filesystem->loadFile($eventFile, function ($file) {
                $this->parseXml(
                    $this->xml->loadXml($file)
                );
            });
        }
    }

    /**
     * @param SimpleXMLElement $xml
     * @throws EventExists
     */
    private function parseXml(SimpleXMLElement $xml)
    {
        foreach ($xml->children() as $event) {
            /** @var SimpleXMLElement $event */
            foreach ($event->children() as $observer) {
                /** @var SimpleXMLElement $observer */
                if (isset(
                    $this->observers[$event->getAttribute('name')][$observer->getAttribute('name')]
                )) {
                    throw new EventExists(
                        sprintf('Observer with name: %s already exists!', $observer->getAttribute('name'))
                    );
                }
                $this->observers[$event->getAttribute('name')][
                    $observer->getAttribute('name')
                ] = $observer->getAttribute('class');
            }
        }
    }
}