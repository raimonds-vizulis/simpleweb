<?php

namespace SimpleWeb\Framework\Events\Observer;

use SimpleWeb\Framework\Events\Model\Event;

interface ObserverInterface
{
    /**
     * @param Event $event
     */
    public function execute(Event $event);
}