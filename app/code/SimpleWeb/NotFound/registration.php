<?php

use SimpleWeb\Framework\Module\Register;

Register::register(
    Register::MODULE,
    'SimpleWeb_NotFound'
);