<?php

namespace SimpleWeb\NotFound\Controller\Index;

use SimpleWeb\Framework\App\Http\Action;

class Index extends Action
{
    public function execute()
    {
        echo 'Not Found!';
    }
}