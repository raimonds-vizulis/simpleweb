<?php

namespace SimpleWeb\Urn\Console;

use DOMDocument;
use Exception;
use SimpleWeb\Framework\App\Util\DirectoryList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateUrn extends Command
{
    /**
     * @var array
     */
    private $resources = [];

    /**
     * @var DirectoryList
     */
    private $directoryList;

    public function __construct(
        DirectoryList $directoryList
    ) {
        parent::__construct();

        $this->directoryList = $directoryList;
    }

    protected function configure()
    {
        $this
            ->setName('dev:urn:generate')
            ->setDescription('Generate XML validation (XSD)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $locations = [
            $this->directoryList->getPath('app/code/SimpleWeb/Framework/*/schema/*.xsd'),
            $this->directoryList->getPath('/app/code/*/*/schema/*.xsd')
        ];

        $file = $this->directoryList->getPath('.idea/misc.xml');

        if (!file_exists($file)) {
            $this->createResourceFile($file);
        }

        /** @var \SimpleXMLElement $resourceFile */
        /** @var \SimpleXMLElement $resourceNode */
        list($resourceFile, $resourceNode) = $this->getResourceNode($file);

        $output->writeln('<info>Generating URN\'s...</info>');

        foreach ($locations as $location) {
            foreach (glob($location) as $item) {
                if ($this->itemExists($resourceNode, $item)) {
                    continue;
                }

                $resource = $resourceNode->addChild('resource');
                $resource->addAttribute('url', $this->getUrl($item));
                $resource->addAttribute('location', $this->getLocation($item));
            }
        }

        $this->saveResourceFile($file, $resourceFile);

        $output->writeln('<info>URN\'s generated successfully!</info>');
    }

    /**
     * @param $file
     * @throws Exception
     */
    private function createResourceFile($file)
    {
        $content = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<project version="1">
    <component name="ProjectResources"/>
</project>
EOF;
        if (!file_put_contents($file, $content)) {
            throw new Exception('Failed to create project resource file!');
        }
    }

    /**
     * Loading XML as DOMDocument to preserve well formatted XML
     *
     * @param $file
     * @param \SimpleXMLElement $resourceFile
     * @throws Exception
     */
    private function saveResourceFile($file, \SimpleXMLElement $resourceFile)
    {
        $dom = new DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($resourceFile->asXML());

        if (!file_put_contents($file, $dom->saveXML())) {
            throw new Exception('Failed to save resource file!');
        }
    }

    /**
     * @param $file
     * @return array
     * @throws Exception
     */
    private function getResourceNode($file)
    {
        $resourceFile = simplexml_load_file($file);

        foreach ($resourceFile->component as $component) {
            if ($this->getXmlAttribute($component, 'name') == 'ProjectResources') {
                return [$resourceFile, $component];
            }
        }

        throw new Exception('Resource file seems to be broken?');
    }

    /**
     * @param $object
     * @param $attribute
     * @return null|string
     */
    private function getXmlAttribute($object, $attribute)
    {
        return isset($object[$attribute]) ? (string) $object[$attribute] : null;
    }

    /**
     * @param $resourceNode
     * @param $item
     * @return bool
     */
    private function itemExists($resourceNode, $item)
    {
        if (empty($this->resources)) {
            foreach ($resourceNode->resource as $resource) {
                $this->resources[] = $this->getXmlAttribute($resource, 'url');
            }
        }

        return in_array($this->getUrl($item), $this->resources);
    }

    /**
     * @param $item
     * @return string
     */
    private function getUrl($item)
    {
        $values = explode('/', $item);
        return 'urn:simpleweb:schema/' . end($values);
    }

    /**
     * @param $item
     * @return mixed
     */
    private function getLocation($item)
    {
        return str_replace($this->directoryList->getPath(), '$PROJECT_DIR$', $item);
    }
}
