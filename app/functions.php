<?php

use SimpleWeb\Framework\App\Localization\Translate;
use SimpleWeb\Framework\Di\Container;

function __($string)
{
    $args = func_get_args();
    $string = array_shift($args);

    /** @var Translate $translator */
    $translator = Container::getInstance()->get(Translate::class);

    return $translator->translate($string, $args);
}