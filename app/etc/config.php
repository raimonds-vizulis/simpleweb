<?php

return [
    'container' => [
        'interfaces' => [
            \SimpleWeb\Framework\App\Http\RequestInterface::class => \SimpleWeb\Framework\App\Http\Request::class,
            \SimpleWeb\Framework\Events\Model\EventDispatcherInterface::class => \SimpleWeb\Framework\Events\Model\EventDispatcher::class,
            \SimpleWeb\Framework\App\SessionInterface::class => \SimpleWeb\Framework\App\Session::class
        ],
        'singletons' => [
            \SimpleWeb\Framework\Di\Container::class,
            \SimpleWeb\Framework\Config\Config::class,
            \SimpleWeb\Framework\App\Http\Util\Routes::class,
            \SimpleWeb\Framework\Events\Model\EventDispatcher::class,
            \SimpleWeb\Framework\App\Session::class,
            \SimpleWeb\Framework\App\Util\DirectoryList::class,
            \SimpleWeb\Framework\Module\Loader::class,
            \SimpleWeb\Framework\Module\Register::class
        ]
    ],
    'config' => [
        'ROOT_DIR' => dirname(dirname(__DIR__))
    ]
];